# Installation

### Install required php extensions

apt install php7.2-json php7.2-amqp

### Install required packages
Run:

composer install

Manually download and extract in vendor folder https://github.com/socialtechio/slow-storage-emulator package

Run:

composer dump-autoload

### Setup api doc

bin/console assets:install --symlink

You can see api doc on 

http://*your-domain.com*/api/doc

### Run queue consumers
Run:

 bin/console rabbitmq:consumer user
 
 bin/console rabbitmq:consumer tracking
