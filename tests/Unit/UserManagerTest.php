<?php


namespace App\Tests\Unit;


use App\Component\Business\User\Factory\UserFactory;
use App\Component\Business\User\UserManager;
use App\Component\Foundation\Queue\Producer\UserProducer;
use App\Component\Foundation\Storage\Helper\IndexStorage;
use App\Component\Foundation\Storage\StorageManager;
use App\Entity\User;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UserManagerTest extends TestCase
{

    /**
     * @var \PHPUnit\Framework\MockObject\MockObject
     */
    private $userFactory;
    /**
     * @var \PHPUnit\Framework\MockObject\MockObject
     */
    private $storageManager;
    /**
     * @var \PHPUnit\Framework\MockObject\MockObject
     */
    private $eventDispatcher;
    /**
     * @var \PHPUnit\Framework\MockObject\MockObject
     */
    private $validator;
    /**
     * @var \PHPUnit\Framework\MockObject\MockObject
     */
    private $indexStorage;
    /**
     * @var \PHPUnit\Framework\MockObject\MockObject
     */
    private $producer;
    /**
     * @var UserManager
     */
    private $userManager;

    public function testCreateUser()
    {
        $data = [
            'nickname' => 'qwerty',
            'password' => 'qwerty',
            'firstName' => 'qwerty',
            'lastName' => 'qwerty',
            'age' => 18,
        ];

        $user = $this->createMock(User::class);

        $this->userFactory->expects($this->once())->method('create')->willReturn($user);

        $this->storageManager->expects($this->once())->method('exist')->willReturn(false);

        $this->producer->expects($this->once())->method('publish');

        $this->eventDispatcher->expects($this->once())->method('dispatch');

        $this->validator->expects($this->once())->method('validate')->willReturn([]);

        $result = $this->userManager->createUser($data);

        $this->assertInstanceOf(User::class, $result);
    }

    public function testUpdateUser()
    {
        $user = $this->createMock(User::class);

        $this->producer->expects($this->once())->method('publish');

        $result = $this->userManager->updateUser($user);

        $this->assertInstanceOf(User::class, $result);
    }

    protected function setUp()
    {
        $this->userFactory = $this->createMock(UserFactory::class);
        $this->storageManager = $this->createMock(StorageManager::class);
        $this->eventDispatcher = $this->createMock(EventDispatcherInterface::class);
        $this->validator = $this->createMock(ValidatorInterface::class);
        $this->indexStorage = $this->createMock(IndexStorage::class);
        $this->producer = $this->createMock(UserProducer::class);
        $container = $this->createMock(ContainerInterface::class);

        $container->expects($this->once())->method('get')->willReturn($this->producer);

        $this->userManager = new UserManager(
            $this->userFactory,
            $this->storageManager,
            $this->eventDispatcher,
            $this->validator,
            $this->indexStorage,
            $container
        );
    }
}