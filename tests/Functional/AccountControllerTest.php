<?php


namespace App\Tests\Functional;


use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\AbstractBrowser;

class AccountControllerTest extends WebTestCase
{

    public function testGetAccountActionAccessDeny()
    {
        /** @var  AbstractBrowser $client */
        $client = static::createClient();

        $client->request(
            'GET',
            '/api/user/account',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json']
        );

        $this->assertEquals(401, $client->getResponse()->getStatusCode());
    }
}