<?php


namespace App\Tests\Functional;


use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\AbstractBrowser;

class TrackingControllerTest extends WebTestCase
{

    public function testTrackAction()
    {
        /** @var  AbstractBrowser $client */
        $client = static::createClient();

        $data = [
            'userId' => 1,
            'sourceLabel' => 'search_page',
        ];

        $client->request(
            'POST',
            '/api/tracking',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode($data)
        );

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
}