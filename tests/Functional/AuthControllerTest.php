<?php


namespace App\Tests\Functional;


use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\AbstractBrowser;

class AuthControllerTest extends WebTestCase
{

    public function testRegisterAction()
    {
        /** @var  AbstractBrowser $client */
        $client = static::createClient();

        $data = [
            'nickname' => 'qwerty',
            'password' => 'qwerty',
            'firstName' => 'qwerty',
            'lastName' => 'qwerty',
            'age' => 18,
        ];

        $client->request(
            'POST',
            '/api/register',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode($data)
        );

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testRegisterActionValidationError()
    {
        /** @var  AbstractBrowser $client */
        $client = static::createClient();

        $data = [
            'nickname' => 'qwe',
            'password' => 'qwerty',
            'firstName' => 'qwerty',
            'lastName' => 'qwerty',
            'age' => 18,
        ];

        $client->request(
            'POST',
            '/api/register',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode($data)
        );

        $this->assertEquals(400, $client->getResponse()->getStatusCode());
    }

    public function testLoginAction()
    {
        /** @var  AbstractBrowser $client */
        $client = static::createClient();

        sleep(6);

        $data = [
            'nickname' => 'qwerty',
            'password' => 'qwerty',
        ];

        $client->request(
            'POST',
            '/api/login',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode($data)
        );

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testLoginActionError()
    {
        /** @var  AbstractBrowser $client */
        $client = static::createClient();

        $data = [
            'nickname' => 'qwe',
            'password' => 'qwerty',
        ];

        $client->request(
            'POST',
            '/api/login',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode($data)
        );

        $this->assertEquals(400, $client->getResponse()->getStatusCode());
    }
}