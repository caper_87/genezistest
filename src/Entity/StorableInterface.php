<?php


namespace App\Entity;


interface StorableInterface
{
    public function getId(): int;

    public function setId(int $id);

    public function getUniqueKey(): string;

    public static function getEntityName():string;
}