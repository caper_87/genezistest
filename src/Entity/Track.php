<?php


namespace App\Entity;


use App\Entity\Mixin\Id;
use JMS\Serializer\Annotation as JMS;

/**
 * Class Track
 * @package App\Entity
 * @JMS\ExclusionPolicy("all")
 */
class Track  implements StorableInterface, \JsonSerializable
{
    use Id;

    /**
     * @var integer
     * @JMS\Groups({"default"})
     * @JMS\Type("integer")
     * @JMS\Expose
     */
    private $userId;

    /**
     * @var string
     * @JMS\Groups({"default"})
     * @JMS\Type("string")
     * @JMS\Expose
     */
    private $sourceLabel;

    /**
     * @var \DateTimeInterface
     * @JMS\Groups({"default"})
     * @JMS\Type("DateTime<'Y-m-d'>")
     * @JMS\Expose
     */
    private $dateCreated;

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     * @return Track
     */
    public function setUserId(int $userId): Track
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @return string
     */
    public function getSourceLabel(): string
    {
        return $this->sourceLabel;
    }

    /**
     * @param string $sourceLabel
     * @return Track
     */
    public function setSourceLabel(string $sourceLabel): Track
    {
        $this->sourceLabel = $sourceLabel;
        return $this;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getDateCreated(): \DateTimeInterface
    {
        return $this->dateCreated;
    }

    /**
     * @param \DateTimeInterface $dateCreated
     * @return Track
     */
    public function setDateCreated(\DateTimeInterface $dateCreated): Track
    {
        $this->dateCreated = $dateCreated;
        return $this;
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'userId' => $this->userId,
            'sourceLabel' => $this->sourceLabel,
            'dateCreated' => $this->dateCreated->format('Y-m-d'),
        ];
    }

    public function getUniqueKey(): string
    {
        return (string) $this->id;
    }

    public static function getEntityName():string
    {
        return 'Track';
    }
}