<?php declare(strict_types=1);


namespace App\Entity;


use App\Entity\Mixin\Id;
use JMS\Serializer\Annotation as JMS;

/**
 * Class Account
 * @package App\Entity
 * @JMS\ExclusionPolicy("all")
 */
class Account implements StorableInterface, \JsonSerializable
{
    use Id;

    /**
     * @var string
     * @JMS\Groups({"default", "view"})
     * @JMS\Type("string")
     * @JMS\Expose
     */
    private $firstName;

    /**
     * @var string
     * @JMS\Groups({"default", "view"})
     * @JMS\Type("string")
     * @JMS\Expose
     */
    private $lastName;


    /**
     * @var integer
     * @JMS\Groups({"default", "view"})
     * @JMS\Type("integer")
     * @JMS\Expose
     */
    private $age;

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     * @return Account
     */
    public function setFirstName(string $firstName): Account
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     * @return Account
     */
    public function setLastName(string $lastName): Account
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * @return int
     */
    public function getAge(): int
    {
        return $this->age;
    }

    /**
     * @param int $age
     * @return Account
     */
    public function setAge(int $age): Account
    {
        $this->age = $age;
        return $this;
    }

    public function getUniqueKey(): string
    {
        return (string) $this->id;
    }

    public static function getEntityName(): string
    {
        return 'Account';
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'nickname' => $this->firstName,
            'password' => $this->lastName,
            'age' => $this->age,
        ];
    }
}