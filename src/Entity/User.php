<?php declare(strict_types=1);


namespace App\Entity;

use App\Entity\Mixin\Id;
use Symfony\Component\Security\Core\User\UserInterface;
use JMS\Serializer\Annotation as JMS;

/**
 * Class User
 * @package App\Entity
 * @JMS\ExclusionPolicy("all")
 */
class User implements UserInterface, \JsonSerializable, StorableInterface
{
    use Id;

    const DEFAULT_ROLE = 'ROLE_USER';

    const SALT = 'change_me';

    /**
     * @var string
     * @JMS\Groups({"default"})
     * @JMS\Type("string")
     * @JMS\Expose
     */
    private $nickname;

    /**
     * @var string
     * @JMS\Groups({"default"})
     * @JMS\Type("string")
     * @JMS\Expose
     */
    private $password;

    /**
     * @var string
     * @JMS\Groups({"default"})
     * @JMS\Type("string")
     * @JMS\Expose
     */
    private $accessToken;

    /**
     * @var Account
     * @JMS\Groups({"default"})
     * @JMS\Type("App\Entity\Account")
     * @JMS\Expose
     */
    private $account;

    /**
     * @return string
     */
    public function getNickname(): string
    {
        return $this->nickname;
    }

    /**
     * @param string $nickname
     * @return User
     */
    public function setNickname(string $nickname): User
    {
        $this->nickname = $nickname;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return User
     */
    public function setPassword(string $password): User
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return string
     */
    public function getAccessToken(): string
    {
        return $this->accessToken;
    }

    /**
     * @param string $accessToken
     * @return User
     */
    public function setAccessToken(string $accessToken): User
    {
        $this->accessToken = $accessToken;
        return $this;
    }

    /**
     * @return Account
     */
    public function getAccount():Account
    {
        return $this->account;
    }

    /**
     * @param mixed $account
     * @return User
     */
    public function setAccount(Account $account): User
    {
        $this->account = $account;
        return $this;
    }


    public function getRoles():array
    {
        return [self::DEFAULT_ROLE];
    }

    public function getSalt(): string
    {
        return self::SALT;
    }

    public function getUsername(): string
    {
        return $this->nickname;
    }

    public function eraseCredentials()
    {
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'nickname' => $this->nickname,
            'password' => $this->password,
            'accessToken' => $this->accessToken,
            'account' => [
                'id' => $this->id,
                'firstName' => $this->getAccount()->getFirstName(),
                'lastName' => $this->getAccount()->getLastName(),
                'age' => $this->getAccount()->getAge(),
            ]
        ];
    }

    public function getUniqueKey(): string
    {
        return $this->nickname;
    }

    public static function getEntityName():string
    {
        return 'User';
    }
}