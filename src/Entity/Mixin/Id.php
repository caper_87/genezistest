<?php


namespace App\Entity\Mixin;

use JMS\Serializer\Annotation as JMS;

trait Id
{
    /**
     * @var int
     * @JMS\Groups({"default"})
     * @JMS\Type("integer")
     * @JMS\Expose
     */
    private $id;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId(int $id)
    {
        $this->id = $id;
        return $this;
    }
}