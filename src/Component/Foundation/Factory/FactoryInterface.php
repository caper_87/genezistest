<?php


namespace App\Component\Foundation\Factory;


use App\Entity\StorableInterface;

interface FactoryInterface
{
    public function create(array $data): StorableInterface;
}