<?php


namespace App\Component\Foundation\Storage;


use App\Entity\StorableInterface;

class StorageManager
{
    private $driverFactory;

    public function __construct(DriverFactory $factory, string $defaultDriver)
    {
        $this->driverFactory = $factory;
        $this->driverFactory->setUpDriver($defaultDriver);
    }

    public function exist(string $key, string $entityName):bool
    {
        $storage = $this->getStorage();
        return $storage->exist($key, $entityName);
    }

    public function retrieve(string $key, string $entityName):StorableInterface
    {
        return $this->getStorage()->retrieve($key, $entityName);
    }

    public function store(StorableInterface $entity)
    {
        $storage = $this->getStorage();

        if ($storage->exist($entity->getUniqueKey(), $entity::getEntityName())) {
            $storage->update($entity);
        } else {
            $storage->insert($entity);
        }
    }

    private function getStorage():StorageInterface
    {
        return $this->driverFactory->make();
    }
}