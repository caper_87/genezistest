<?php


namespace App\Component\Foundation\Storage;


interface StorageDriverInterface
{
    public function getDriverId(): string;
}