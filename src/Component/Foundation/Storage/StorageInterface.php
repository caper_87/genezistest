<?php


namespace App\Component\Foundation\Storage;


use App\Entity\StorableInterface;

interface StorageInterface
{
    public function retrieve(string $key, string $entityName):StorableInterface;

    public function insert(StorableInterface $entity);

    public function update(StorableInterface $entity);

    public function exist(string $key, string $entityName):bool;
}