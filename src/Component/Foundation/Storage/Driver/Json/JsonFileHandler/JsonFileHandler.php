<?php


namespace App\Component\Foundation\Storage\Driver\Json\JsonFileHandler;


use App\Component\Foundation\Storage\Driver\Json\FileHandlerInterface;

class JsonFileHandler implements FileHandlerInterface
{
    const EXT = '.json';

    private $path;

    private $prefix;

    public function __construct(string $path)
    {
        $this->path = $path;
        $this->prefix = '';
    }

    public function setPrefix(string $prefix):FileHandlerInterface
    {
        $this->prefix = $prefix . '/';
        return $this;
    }

    public function initIfNotExist(string $fileName,  string $content = '')
    {
        if (!$this->isFileExist($fileName)) {
            $path = $this->path . $this->prefix;
            if (!file_exists($path)) {
                mkdir($this->path . $this->prefix, 0777, true);
            }
            $this->putFileContent($fileName, $content);
        }
    }

    public function isFileExist(string $fileName): bool
    {
        return file_exists($this->path . $this->prefix . $fileName . self::EXT);
    }

    public function find(string $fileName):string
    {
        if (!$this->isFileExist($fileName)) {
            throw new \Exception("File {$fileName} not found!");
        }
        return $this->getFileContent($fileName);
    }

    public function put(string $fileName, string $content)
    {
        $this->putFileContent($fileName, $content);
    }

    public function deleteFile(string $fileName)
    {
        if (!$this->isFileExist($fileName)) {
            throw new \Exception("File {$fileName} not found!");
        }
        unlink($this->path . $this->prefix . $fileName . self::EXT);
    }

    private function getFileContent(string $fileName):string
    {
        return file_get_contents($this->path . $this->prefix . $fileName . self::EXT);
    }

    private function putFileContent(string $fileName, string $content)
    {
        $fp = fopen($this->path . $this->prefix. $fileName . self::EXT, "w+");

        if (flock($fp, LOCK_EX)) {
            ftruncate($fp, 0);
            fwrite($fp, $content);
            fflush($fp);
            flock($fp, LOCK_UN);
        } else {
            throw new \Exception("File {$fileName} lock error!");
        }

        fclose($fp);
    }
}