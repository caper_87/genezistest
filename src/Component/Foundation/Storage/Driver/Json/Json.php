<?php


namespace App\Component\Foundation\Storage\Driver\Json;


use App\Component\Foundation\Storage\StorageDriverInterface;
use App\Component\Foundation\Storage\StorageInterface;
use App\Entity\StorableInterface;
use App\Entity\User;
use JMS\Serializer\SerializerInterface;
use SocialTech\SlowStorage;

class Json implements StorageInterface, StorageDriverInterface
{
    const DRIVER_ID = 'json';
    const SERIALIZE_FORMAT = 'json';

    private $fileStorage;

    private $serializer;

    private $path;

    public  function __construct(
        SerializerInterface $serializer,
        string $path
    )
    {
        $this->fileStorage = new SlowStorage();
        $this->serializer = $serializer;
        $this->path = $path;
    }

    public function getDriverId(): string
    {
        return self::DRIVER_ID;
    }

    public function exist(string $key, string $entityName): bool
    {
        $path = $this->getStoragePath($entityName, $key);
        return $this->fileStorage->exists($path);
    }

    public function retrieve(string $key, string $entityName): StorableInterface
    {
        $path = $this->getStoragePath($entityName, $key);
        $content = $this->fileStorage->load($path);
        return $this->unserialize($content, User::class);
    }
    
    public function insert(StorableInterface $entity)
    {
        $path = $this->getStoragePath($entity::getEntityName(), $entity->getUniqueKey());
        $content = $this->serialize($entity);
        $this->fileStorage->store($path, $content);
    }

    public function update(StorableInterface $entity)
    {
        $path = $this->getStoragePath($entity::getEntityName(), $entity->getUniqueKey());
        $content = $this->serialize($entity);
        $this->fileStorage->store($path, $content);
    }

    private function serialize($data)
    {
        return $this->serializer->serialize($data, self::SERIALIZE_FORMAT);
    }

    private function unserialize(string $content, string $type)
    {
        return $data = $this->serializer->deserialize($content, $type, self::SERIALIZE_FORMAT);
    }

    private function getStoragePath(string $prefix, string $key):string
    {
        $folder = $this->path . $prefix.'/';
        if (!file_exists($folder)) {
            mkdir($folder, 0777, true);
        }

        return $folder . $key . '.json';
    }
}