<?php


namespace App\Component\Foundation\Storage\Driver\Json;


interface FileHandlerInterface
{
    public function initIfNotExist(string $fileName,  string $content = '');

    public function isFileExist(string $fileName): bool;

    public function find(string $fileName):string;

    public function put(string $fileName, string $content);

    public function deleteFile(string $fileName);

    public function setPrefix(string $prefix):FileHandlerInterface;
}