<?php


namespace App\Component\Foundation\Storage\Helper;


use App\Entity\StorableInterface;

class IndexStorage
{
    const STORAGE_FOLDER = 'index/';
    const EXT = '.json';

    private $path;

    public function __construct(string $path)
    {
        $this->path = $path;
    }

    public function load(string $idxName):array
    {
        $this->prepareFolder();
        return $this->loadIndexStorage($idxName);
    }

    public function store(StorableInterface $entity, array $fields)
    {
        $this->prepareFolder();

        $idxName = $entity::getEntityName();

        $data = $this->loadIndexStorage($idxName);

        $data[$entity->getUniqueKey()] = $fields;

        $content = $this->serialize($data);
        file_put_contents($this->getPKStoragePath($idxName), $content, LOCK_EX);
    }

    private function loadIndexStorage(string $idxName):array
    {
        if (!file_exists($this->getPKStoragePath($idxName))) {
            return [];
        }
        $content = file_get_contents($this->getPKStoragePath($idxName));
        return $this->unserialize($content);
    }

    private function prepareFolder()
    {
        if (!file_exists($this->getPKStorageFolder())) {
            mkdir($this->getPKStorageFolder(), 0777, true);
        }
    }

    private function getPKStoragePath(string $prefix):string
    {
        return $this->path . self::STORAGE_FOLDER . $prefix . self::EXT;
    }

    private function getPKStorageFolder():string
    {
        return $this->path . self::STORAGE_FOLDER;
    }

    private function serialize($data): string
    {
        return json_encode($data);
    }

    private function unserialize(string $content, bool $assoc = true)
    {
        return json_decode($content, $assoc);
    }
}