<?php


namespace App\Component\Foundation\Storage\Helper;


use App\Entity\StorableInterface;

class PrimaryKeyFactory
{
    const PKS_STORAGE_FOLDER = 'pks/';
    const EXT = '.json';
    const START_POINT = 0;

    private $path;

    public function __construct(string $path)
    {
        $this->path = $path;
    }

    public function makeId(StorableInterface $entity)
    {
        $prefix = $entity::getEntityName();
        $lastId = $this->getLastInsertId($prefix);
        $pk = ++$lastId;

        $content = $this->serialize(['lastInsertId' => $pk]);
        file_put_contents($this->getPKStoragePath($prefix), $content, LOCK_EX);

        return $pk;
    }

    private function getLastInsertId(string $prefix): int
    {
        $this->prepareFolder();

        if (!file_exists($this->getPKStoragePath($prefix))) {
            $content = $this->serialize(['lastInsertId' => self::START_POINT]);
            file_put_contents($this->getPKStoragePath($prefix), $content, LOCK_EX);
            return self::START_POINT;
        }

        $pks = file_get_contents($this->getPKStoragePath($prefix));
        $lastInsertId = $this->unserialize($pks, false)->lastInsertId;
        return (int) $lastInsertId;
    }

    private function prepareFolder()
    {
        if (!file_exists($this->getPKStorageFolder())) {
            mkdir($this->getPKStorageFolder(), 0777, true);
        }
    }

    private function getPKStoragePath(string $prefix):string
    {
        return $this->path . self::PKS_STORAGE_FOLDER . $prefix . self::EXT;
    }

    private function getPKStorageFolder():string
    {
        return $this->path . self::PKS_STORAGE_FOLDER;
    }

    private function serialize($data): string
    {
        return json_encode($data);
    }

    private function unserialize(string $content, bool $assoc)
    {
        return json_decode($content, $assoc);
    }
}