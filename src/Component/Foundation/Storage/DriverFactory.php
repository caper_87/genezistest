<?php


namespace App\Component\Foundation\Storage;


class DriverFactory
{
    /** @var StorageInterface  */
    private $drivers;

    private $currentDriver;

    public function __construct(iterable $drivers)
    {
        foreach ($drivers as $driver) {
            $this->drivers[] = $driver;
        }
    }

    public function make(): StorageInterface
    {
        foreach ($this->drivers as $driver) {
            if ($driver->getDriverId() === $this->currentDriver) {
                return $driver;
            }
        }
        throw new \RuntimeException("Undefined driver {$this->currentDriver}!");
    }

    public function setUpDriver(string $driverId): self
    {
        $this->currentDriver = $driverId;
        return $this;
    }
}