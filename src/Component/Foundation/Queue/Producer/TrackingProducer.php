<?php


namespace App\Component\Foundation\Queue\Producer;


use OldSound\RabbitMqBundle\RabbitMq\Producer;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AbstractConnection;

class TrackingProducer extends Producer
{
    public function __construct(AbstractConnection $conn, AMQPChannel $ch = null, $consumerTag = null)
    {
        parent::__construct($conn, $ch, $consumerTag);
        parent::setContentType("application/json");
    }
}