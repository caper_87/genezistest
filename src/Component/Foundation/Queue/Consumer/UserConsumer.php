<?php


namespace App\Component\Foundation\Queue\Consumer;


use App\Component\Foundation\Storage\StorageManager;
use App\Entity\User;
use JMS\Serializer\SerializerInterface;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use Psr\Log\LoggerInterface;

class UserConsumer implements ConsumerInterface
{
    private $storageManager;
    private $logger;
    private $serializer;

    public function __construct(
        StorageManager $storageManager,
        LoggerInterface $logger,
        SerializerInterface $serializer
    )
    {
        $this->storageManager = $storageManager;
        $this->logger = $logger;
        $this->serializer = $serializer;
    }

    public function execute(AMQPMessage $msg)
    {
        $body = $msg->getBody();
        $entity = $this->getEntity($body);
        $this->storageManager->store($entity);
        $this->logger->info("QUEUE: " . $body);
    }

    private function getEntity(string $body)
    {
        return $this->serializer->deserialize($body, User::class, 'json');
    }

}