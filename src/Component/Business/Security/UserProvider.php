<?php


namespace App\Component\Business\Security;


use App\Component\Business\User\UserManager;
use App\Entity\User;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class UserProvider implements UserProviderInterface
{
    private $userManager;

    public function __construct(UserManager $userManager)
    {
        $this->userManager = $userManager;
    }

    /**
     * @param string $username
     * @return \App\Entity\StorableInterface|UserInterface
     * @throws \Exception
     */
    public function loadUserByUsername($username):UserInterface
    {
        return $this->userManager->retrieveUser($username);
    }

    /**
     * @param UserInterface $user
     * @return \App\Entity\StorableInterface|UserInterface
     * @throws \Exception
     */
    public function refreshUser(UserInterface $user):UserInterface
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Invalid user class "%s".', get_class($user)));
        }

        return $this->userManager->retrieveUser($user->getUniqueKey());
    }

    /**
     * Tells Symfony to use this provider for this User class.
     */
    public function supportsClass($class):bool
    {
        return User::class === $class;
    }
}