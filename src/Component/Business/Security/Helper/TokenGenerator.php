<?php


namespace App\Component\Business\Security\Helper;


trait TokenGenerator
{
    public function generateToken()
    {
        return sha1(random_bytes(1000) . microtime());
    }

}