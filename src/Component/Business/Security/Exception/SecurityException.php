<?php


namespace App\Component\Business\Security\Exception;


use App\Exception\RuntimeException;

class SecurityException extends RuntimeException
{

}