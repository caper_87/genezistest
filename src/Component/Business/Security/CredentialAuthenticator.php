<?php

namespace App\Component\Business\Security;


use App\Component\Business\Security\Exception\SecurityException;
use App\Component\Business\Security\Helper\TokenGenerator;
use App\Component\Business\User\UserManager;
use App\Controller\Api\Normalizer\UserNormalizer;
use App\Entity\User;
use App\Event\Event\UserEvent;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;

class CredentialAuthenticator extends AbstractGuardAuthenticator
{
    use TokenGenerator;

    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    private $userManager;

    private $eventDispatcher;

    public function __construct(
        UserPasswordEncoderInterface $passwordEncoder,
        SerializerInterface $serializer,
        UserManager $userManager,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->passwordEncoder = $passwordEncoder;
        $this->serializer = $serializer;
        $this->userManager = $userManager;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function supports(Request $request)
    {
        return'login' === $request->attributes->get('_route')
            && $request->isMethod('POST');
    }

    /**
     * Returns a response that directs the user to authenticate.
     * @param Request $request The request that resulted in an AuthenticationException
     * @param AuthenticationException $authException The exception that started the authentication process
     * @return Response
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        return new JsonResponse(null, Response::HTTP_UNAUTHORIZED);
    }

    /**
     * Get the authentication credentials from the request and return them
     * as any type (e.g. an associate array). If you return null, authentication
     * will be skipped.
     * @param Request $request
     * @return mixed|null
     */
    public function getCredentials(Request $request)
    {
        $result = [
            'nickname' => $request->request->get('nickname'),
            'password' => $request->request->get('password')
        ];

        return $result;
    }

    /**
     * Return a UserInterface object based on the credentials.
     * @param mixed $credentials
     * @param UserProviderInterface $userProvider
     * @throws AuthenticationException
     * @return UserInterface|null
     */
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        if (!isset($credentials['nickname']) || !isset($credentials['password'])) {
            throw new SecurityException('Wrong or missing credentials!');
        }

        return $userProvider->loadUserByUsername($credentials['nickname']);
    }

    /**
     * Returns true if the credentials are valid.
     * @param mixed $credentials
     * @param UserInterface $user
     * @return bool
     * @throws AuthenticationException
     */
    public function checkCredentials($credentials, UserInterface $user)
    {
        if (!$user) {
            return false;
        }

        $result = $this->passwordEncoder->isPasswordValid($user, $credentials['password']);
        return $result;
    }

    /**
     * @param Request $request
     * @param AuthenticationException $exception
     * @return Response|void|null
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $data = [
            'message' => strtr($exception->getMessageKey(), $exception->getMessageData())
        ];

        return new JsonResponse($data, Response::HTTP_FORBIDDEN);
    }

    /**
     * Called when authentication executed and was successful!
     * @param Request $request
     * @param TokenInterface $token
     * @param string $providerKey The provider (i.e. firewall) key
     *
     * @return Response|null
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        $newToken = $this->generateToken();

        /** @var User $user */
        $user = $token->getUser();
        $user->setAccessToken($newToken);

        $this->userManager->updateUser($user);

        $this->eventDispatcher->dispatch(new UserEvent($user), UserEvent::SIGNED_UP);

        return new JsonResponse([
            'token' => $newToken,
            'user' => UserNormalizer::normalize($user)
        ]);
    }

    /**
     * @return bool
     */
    public function supportsRememberMe()
    {
        return false;
    }
}