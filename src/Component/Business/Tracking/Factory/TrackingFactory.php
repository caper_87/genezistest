<?php


namespace App\Component\Business\Tracking\Factory;


use App\Component\Business\Security\Helper\TokenGenerator;
use App\Component\Foundation\Factory\FactoryInterface;
use App\Component\Foundation\Storage\Helper\PrimaryKeyFactory;
use App\Entity\StorableInterface;
use App\Entity\Track;
use App\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class TrackingFactory implements FactoryInterface
{
    use TokenGenerator;

    private $accountFactory;
    private $passwordEncoder;
    private $primaryKeyFactory;

    public function __construct(
        PrimaryKeyFactory $primaryKeyFactory
    )
    {
        $this->primaryKeyFactory = $primaryKeyFactory;
    }

    public function create(array $data): StorableInterface
    {
        $track = new Track();
        $id = $this->primaryKeyFactory->makeId($track);

        $track->setId($id)
            ->setUserId($data['userId'])
            ->setSourceLabel($data['sourceLabel'])
            ->setDateCreated(new \DateTime());

        return $track;
    }
}