<?php


namespace App\Component\Business\Tracking;


use App\Component\Business\Tracking\Factory\TrackingFactory;
use App\Component\Business\User\Exception\UserValidationException;
use App\Component\Foundation\Storage\StorageManager;
use App\Entity\StorableInterface;
use App\Entity\Track;
use App\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class TrackingManager
{
    private $factory;
    private $validator;
    private $producer;

    public function __construct(
        TrackingFactory $factory,
        ValidatorInterface $validator,
        ContainerInterface $container
    )
    {
        $this->factory = $factory;
        $this->validator = $validator;
        $this->producer = $container->get('old_sound_rabbit_mq.tracking_producer');
    }

    public function track(User $user, string $sourceLabel): StorableInterface
    {
        $data = [
            'userId' => $user->getId(),
            'sourceLabel' => $sourceLabel
        ];

        $this->validate($data);

        $track = $this->factory->create($data);

        $this->producer->publish(json_encode($track));

        return $track;
    }

    private function validate(array $data)
    {
        $constraint = new Assert\Collection([
            'userId' => new Assert\NotBlank(),
            'sourceLabel' => new Assert\NotBlank(),
        ]);

        $violations = $this->validator->validate($data, $constraint);

        if (0 !== count($violations)) {
            $property = $violations[0]->getPropertyPath();
            $errorMessage = $violations[0]->getMessage();
            throw new UserValidationException("Validation error for {$property}: {$errorMessage}");
        }
    }
}