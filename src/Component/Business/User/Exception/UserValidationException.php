<?php


namespace App\Component\Business\User\Exception;


use App\Exception\ClientException;

class UserValidationException extends ClientException
{

}