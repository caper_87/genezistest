<?php


namespace App\Component\Business\User\Exception;


use App\Exception\ClientException;

class UserNotFoundException extends ClientException
{

}