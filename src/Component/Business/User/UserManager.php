<?php


namespace App\Component\Business\User;


use App\Component\Business\Security\Exception\SecurityException;
use App\Component\Business\User\Exception\UserException;
use App\Component\Business\User\Exception\UserNotFoundException;
use App\Component\Business\User\Exception\UserValidationException;
use App\Component\Business\User\Factory\UserFactory;
use App\Component\Foundation\Storage\Helper\IndexStorage;
use App\Component\Foundation\Storage\StorageManager;
use App\Entity\Track;
use App\Entity\User;
use App\Event\Event\UserEvent;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\Constraints as Assert;

class UserManager
{
    private $userFactory;
    private $storageManager;
    private $eventDispatcher;
    private $validator;
    private $indexStorage;
    private $producer;

    public function __construct(
        UserFactory $userFactory,
        StorageManager $storageManager,
        EventDispatcherInterface $eventDispatcher,
        ValidatorInterface $validator,
        IndexStorage $indexStorage,
        ContainerInterface $container
    )
    {
        $this->userFactory = $userFactory;
        $this->storageManager = $storageManager;
        $this->eventDispatcher = $eventDispatcher;
        $this->validator = $validator;
        $this->indexStorage = $indexStorage;
        $this->producer = $container->get('old_sound_rabbit_mq.user_producer');
    }

    public function retrieveUser(string $nickname):User
    {
        if (!$this->storageManager->exist($nickname, User::getEntityName())) {
            throw new UserNotFoundException('User not found!');
        }
        return $this->storageManager->retrieve($nickname, User::getEntityName());
    }

    public function createUser(array $data): User
    {
        $this->validate($data);

        $user = $this->userFactory->create($data);

        if ($this->storageManager->exist($user->getUniqueKey(),  User::getEntityName())) {
            throw new UserException('User already exist!');
        }

        $this->producer->publish(json_encode($user));

        $this->eventDispatcher->dispatch(new UserEvent($user), UserEvent::CREATED);

        return $user;
    }

    public function updateUser(User $user): User
    {
        $this->producer->publish(json_encode($user));

        return $user;
    }

    public function retrieveUserByToken(string $apiToken): User
    {
        $userIndex = $this->indexStorage->load(User::getEntityName());
        foreach ($userIndex as $nickname => $userData) {
            if ($userData['token'] === $apiToken) {
                return $this->retrieveUser($nickname);
            }
        }
        throw new UserException('User not found!');
    }

    public function retrieveUserById(int $id): User
    {
        $userIndex = $this->indexStorage->load(User::getEntityName());
        foreach ($userIndex as $nickname => $userData) {
            if ($userData['id'] === $id) {
                return $this->retrieveUser($nickname);
            }
        }
        throw new UserException('User not found!');
    }

    private function validate(array $data)
    {
        $constraint = new Assert\Collection([
            'nickname' => new Assert\Length(['min' => 4, 'max' => 50]),
            'firstName' => new Assert\Length(['min' => 2, 'max' => 100]),
            'lastName' => new Assert\Length(['min' => 2, 'max' => 100]),
            'age' => new Assert\GreaterThanOrEqual(1),
            'password' => new Assert\Length(['min' => 6, 'max' => 100]),
        ]);

        $violations = $this->validator->validate($data, $constraint);

        if (0 !== count($violations)) {
            $property = $violations[0]->getPropertyPath();
            $errorMessage = $violations[0]->getMessage();
            throw new UserValidationException("Validation error for {$property}: {$errorMessage}");
        }
    }
}