<?php


namespace App\Component\Business\User\Factory;


use App\Component\Foundation\Factory\FactoryInterface;
use App\Component\Foundation\Storage\Helper\PrimaryKeyFactory;
use App\Entity\Account;
use App\Entity\StorableInterface;

class UserAccountFactory implements FactoryInterface
{
    private $primaryKeyFactory;

    public function __construct(PrimaryKeyFactory $primaryKeyFactory)
    {
        $this->primaryKeyFactory = $primaryKeyFactory;
    }

    public function create(array $data): StorableInterface
    {
        $account = new Account();
        $id = $this->primaryKeyFactory->makeId($account);

        $account->setId($id)
            ->setFirstname($data['firstName'])
            ->setLastname($data['lastName'])
            ->setAge($data['age']);

        return $account;
    }
}