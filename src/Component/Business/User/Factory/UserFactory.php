<?php


namespace App\Component\Business\User\Factory;


use App\Component\Business\Security\Helper\TokenGenerator;
use App\Component\Foundation\Factory\FactoryInterface;
use App\Component\Foundation\Storage\Helper\PrimaryKeyFactory;
use App\Entity\StorableInterface;
use App\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFactory implements FactoryInterface
{
    use TokenGenerator;

    private $accountFactory;
    private $passwordEncoder;
    private $primaryKeyFactory;

    public function __construct(
        PrimaryKeyFactory $primaryKeyFactory,
        UserAccountFactory $accountFactory,
        UserPasswordEncoderInterface $passwordEncoder
    )
    {
        $this->primaryKeyFactory = $primaryKeyFactory;
        $this->accountFactory = $accountFactory;
        $this->passwordEncoder = $passwordEncoder;
    }

    public function create(array $data): StorableInterface
    {
        $user = new User();
        $id = $this->primaryKeyFactory->makeId($user);
        $password = $this->passwordEncoder->encodePassword($user, $data['password']);
        $token = $this->generateToken();
        $account = $this->accountFactory->create($data);

        $user->setId($id)
            ->setNickname($data['nickname'])
            ->setPassword($password)
            ->setAccessToken($token)
            ->setAccount($account);

        return $user;
    }
}