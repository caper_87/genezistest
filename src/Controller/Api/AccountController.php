<?php


namespace App\Controller\Api;

use App\Controller\Api\Normalizer\UserNormalizer;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Security;

/**
 * Class AccountController
 * @package App\Api\Controller
 * @SWG\Tag(name="User Account")
 * @Route("/user/account", name="user.")
 */
class AccountController extends AbstractFOSRestController
{

    /**
     * @Route("", name="get_account", methods={"GET"})
     * @Security(name="Bearer")
     * @SWG\Response(
     *     response=200,
     *     description="Get user Account"
     * )
     */
    public function getAccountAction()
    {
        $user = $this->getUser();
        return new JsonResponse(UserNormalizer::normalize($user));
    }
}