<?php


namespace App\Controller\Api;

use App\Component\Business\Tracking\TrackingManager;
use App\Component\Business\User\UserManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Swagger\Annotations as SWG;

/**
 * Class TrackingController
 * @package App\Api\Controller
 * @SWG\Tag(name="Tracking")
 * @Route("/tracking", name="tracking.")
 */
class TrackingController extends AbstractFOSRestController
{

    /**
     * @Route("", name="track", methods={"POST"})
     * @SWG\Parameter(
     *     name="data",
     *     in="body",
     *     @SWG\Schema(
     *         type="object",
     *          @SWG\Property(property="userId", type="integer", example="123"),
     *          @SWG\Property(property="sourceLabel", type="string", example="search_page")
     *     )
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Track user actions"
     * )
     */
    public function trackAction(Request $request, UserManager $userManager, TrackingManager $trackingManager)
    {
        // trying get user from token(authorized) or by unique id(non authorized)
        if (!$user = $this->getUser()) {
            if ($id = $request->request->getInt('userId')) {
                $user = $userManager->retrieveUserById($id);
            }
        }

        if ($user) {
            $trackingManager->track($user, $request->get('sourceLabel'));
        }

        return new JsonResponse();
    }
}