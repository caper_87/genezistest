<?php


namespace App\Controller\Api;

use App\Component\Business\User\UserManager;
use App\Controller\Api\Normalizer\UserNormalizer;
use FOS\RestBundle\View\View;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Swagger\Annotations as SWG;

/**
 * Class AuthController
 * @package App\Api\Controller
 * @SWG\Tag(name="Auth")
 */
class AuthController extends AbstractFOSRestController
{
    /**
     * @Route("/login", name="login", methods={"POST"})
     * @SWG\Parameter(
     *     name="data",
     *     in="body",
     *     @SWG\Schema(
     *         type="object",
     *          @SWG\Property(property="nickname", type="string", example="john"),
     *          @SWG\Property(property="password", type="string", example="password")
     *     )
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Login user"
     * )
     */
    public function loginAction()
    {

    }

    /**
     * @Route("/register", name="register", methods={"POST"})
     * @SWG\Parameter(
     *     name="data",
     *     in="body",
     *     @SWG\Schema(
     *         type="object",
     *          @SWG\Property(property="nickname", type="string", example="john"),
     *          @SWG\Property(property="password", type="string", example="password"),
     *          @SWG\Property(property="firstName", type="string", example="John"),
     *          @SWG\Property(property="lastName", type="string", example="Smith"),
     *          @SWG\Property(property="age", type="integer", example="28")
     *     )
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Register user"
     * )
     */
    public function registerAction(Request $request, UserManager $userManager)
    {
        $user = $userManager->createUser($request->request->all());

        return new JsonResponse(UserNormalizer::normalize($user));
    }
}