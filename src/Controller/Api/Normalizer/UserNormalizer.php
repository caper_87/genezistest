<?php


namespace App\Controller\Api\Normalizer;


use App\Entity\User;

class UserNormalizer
{
    public static function normalize(User $user):array
    {
        return [
            'id' => $user->getId(),
            'nickname' => $user->getNickname(),
            'account' => [
                'id' => $user->getAccount()->getId(),
                'firstName' => $user->getAccount()->getFirstName(),
                'lastName' => $user->getAccount()->getLastName(),
                'age' => $user->getAccount()->getAge(),
            ]
        ];
    }
}