<?php


namespace App\Event;

use App\Exception\ClientException;
use App\Exception\RuntimeException;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;

class ExceptionListener
{
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function onKernelException(ExceptionEvent $event)
    {
        $exception = $event->getException();

        if ($exception instanceof RuntimeException) {
            $status = Response::HTTP_BAD_REQUEST;
            $message = ['error' => $exception->getMessage()];

            $this->logger->error('API Request exception', [
                'channel' => 'API',
                'exception' => $exception,
                'request' => $event->getRequest()
            ]);
        } elseif ($exception instanceof ClientException) {
            $status = Response::HTTP_BAD_REQUEST;
            $message = ['error' => $exception->getMessage()];
        } else {
            $status = Response::HTTP_INTERNAL_SERVER_ERROR;
            $message = ['error' => $exception->getMessage()];
        }

        $response = new JsonResponse($message, $status);
        $event->setResponse($response);
    }
}