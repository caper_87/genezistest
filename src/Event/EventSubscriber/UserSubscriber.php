<?php


namespace App\Event\EventSubscriber;


use App\Component\Foundation\Storage\Helper\IndexStorage;
use App\Event\Event\UserEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class UserSubscriber implements EventSubscriberInterface
{
    private $indexStorage;

    public function __construct(IndexStorage $indexStorage)
    {
        $this->indexStorage = $indexStorage;
    }

    public static function getSubscribedEvents()
    {
        return [
            UserEvent::CREATED => 'onCreated',
            UserEvent::SIGNED_UP => 'onSignedUp',
        ];
    }

    public function onCreated(UserEvent $event)
    {
        $user = $event->getUser();
        $data = [
            'id' => $user->getId(),
            'token' => $user->getAccessToken()
        ];
        $this->indexStorage->store($user, $data);
    }

    public function onSignedUp(UserEvent $event)
    {
        $user = $event->getUser();
        $data = [
            'id' => $user->getId(),
            'token' => $user->getAccessToken()
        ];
        $this->indexStorage->store($user, $data);
    }

}