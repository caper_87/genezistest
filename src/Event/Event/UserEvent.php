<?php


namespace App\Event\Event;


use App\Entity\User;

class UserEvent
{
    const CREATED = 'user:created';
    const SIGNED_UP = 'user:signed_up';

    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return User
     */
    public function getUser():User
    {
        return $this->user;
    }
}